class NewLeadPage < AbstractPage
  include PageObject
  include RelatedToPicker

  page_url 'https://app.futuresimple.com/leads/new'

  button(:save, :class => 'save btn btn-large btn-primary')
  text_field(:first_name, :id => 'lead-first-name')
  text_field(:last_name, :id => 'lead-last-name')
  text_field(:company, :id => 'lead-company-name')
  text_field(:title, :id => 'lead-title')
  text_field(:email, :id => 'lead-email')
  text_field(:mobile, :id => 'lead-mobile')
  text_field(:phone, :id => 'lead-phone')
  text_field(:street, :id => 'lead-street')
  text_field(:city, :id => 'lead-city')
  text_field(:zip, :id => 'lead-zip')
  text_field(:state, :id => 'lead-region')
  select_list(:country, :class => 'chzn-done')
end
